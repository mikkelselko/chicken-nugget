'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ScrollHandler = require('./ScrollHandler');

var ChickenNugget = function ChickenNugget(_ref) {
  var _ref$destroyAfterAnim = _ref.destroyAfterAnimation,
      destroyAfterAnimation = _ref$destroyAfterAnim === undefined ? false : _ref$destroyAfterAnim,
      _ref$b = _ref.b,
      b = _ref$b === undefined ? 'default b' : _ref$b,
      _ref$c = _ref.c,
      c = _ref$c === undefined ? 'default c' : _ref$c;

  _classCallCheck(this, ChickenNugget);

  this.destroyAfterAnimation = destroyAfterAnimation;
  this.b = b;
  this.c = c;

  console.log('destroyAfterAnimation: ' + this.destroyAfterAnimation);
  console.log('b: ' + this.b);
  console.log('c: ' + this.c);

  var scrollHandler = new ScrollHandler();
  scrollHandler.init();
};

module.exports = ChickenNugget;