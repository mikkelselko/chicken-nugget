'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var anime = require('animejs');

var Animations = function () {
    function Animations() {
        _classCallCheck(this, Animations);
    }

    _createClass(Animations, [{
        key: 'slideInRight',
        value: function slideInRight() {
            var _this = this.watchItem;

            anime({
                targets: _this,
                translateX: [-100, 0],
                duration: 1000
                // opacity: [0, 1]
            });
        }
    }, {
        key: 'slideInLeft',
        value: function slideInLeft() {
            var _this = this.watchItem;

            anime({
                targets: _this,
                translateX: [100, 0],
                duration: 1000
                // opacity: [0, 1]
            });
        }
    }]);

    return Animations;
}();

module.exports = Animations;