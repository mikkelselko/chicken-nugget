'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var scrollMonitor = require('scrollMonitor');
var Animations = require('./Animations');

var ScrollHandler = function () {
    function ScrollHandler() {
        _classCallCheck(this, ScrollHandler);
    }

    _createClass(ScrollHandler, [{
        key: 'init',
        value: function init() {
            var animations = new Animations();
            this.addScrollAnimation('.slide-in-right', 0, animations.slideInRight);
            this.addScrollAnimation('.slide-in-left', 0, animations.slideInLeft);
        }
    }, {
        key: 'createWatcher',
        value: function createWatcher(selector, offset, animation) {
            var watcher = scrollMonitor.create(selector, offset);
            watcher.enterViewport(animation);
        }
    }, {
        key: 'addScrollAnimation',
        value: function addScrollAnimation(selector, offset, animation) {
            if (document.querySelectorAll(selector).length > 0) {
                var myElement = document.querySelectorAll(selector);

                for (var i = myElement.length - 1; i >= 0; i--) {
                    this.createWatcher(myElement[i], offset, animation);
                }
            }
        }
    }]);

    return ScrollHandler;
}();

module.exports = ScrollHandler;