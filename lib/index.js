let ScrollHandler = require('./ScrollHandler');

class ChickenNugget {

  constructor ({destroyAfterAnimation = false, offset = 0, c = 'default c'}) {
    this.destroyAfterAnimation = destroyAfterAnimation;
    this.offset = offset;
    this.c = c;

    console.log('destroyAfterAnimation: ' + this.destroyAfterAnimation);
    console.log('offset: ' + this.offset);
    console.log('c: ' + this.c);

    const scrollHandler = new ScrollHandler();
    scrollHandler.init();
  }

}

module.exports = ChickenNugget;