let scrollMonitor = require('scrollMonitor');
let Animations = require('./Animations');

class ScrollHandler {
    constructor() {}

    init() {
        const animations = new Animations();
        this.addScrollAnimation('.slide-in-right', 0, animations.slideInRight);
        this.addScrollAnimation('.slide-in-left', 0, animations.slideInLeft);
    }

    createWatcher (selector, offset, animation) {
        let watcher = scrollMonitor.create(selector, offset);
        watcher.enterViewport(animation);
    }

    addScrollAnimation (selector, offset, animation) {
        if (document.querySelectorAll(selector).length > 0) { 
            let myElement = document.querySelectorAll(selector);

            for (let i = myElement.length - 1; i >= 0; i--) {
                this.createWatcher(myElement[i], offset, animation);
            }
        }
    }
}

module.exports = ScrollHandler;