const anime = require('animejs');

class Animations {

    constructor() {}

    slideInRight () {
        const _this = this.watchItem;

        anime({
            targets: _this,
            translateX: [-100, 0],
            duration: 1000
            // opacity: [0, 1]
        });
    }
    
    slideInLeft () {
        const _this = this.watchItem;

        anime({
            targets: _this,
            translateX: [100, 0],
            duration: 1000
            // opacity: [0, 1]
        });
    }

}

module.exports = Animations;